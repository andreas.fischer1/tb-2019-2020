---
version: 1
titre: Handwriting Recognition for Parish Records of the Canton of Fribourg
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
attribué à:
  - Nicolas Feyer
professeurs co-superviseurs:
mots-clés: [handwriting recognition, document analysis, machine learning, pattern recognition]
langue: [F,E,D]
confidentialité: non
suite: oui
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth,height=\textheight]{img/hwrfr.png}
\end{center}
```

## Description/Contexte

In this project, an application is being further developed that allows to automatically read scanned parish records of the canton of Fribourg. Such an automatic transcription has many applications, including genealogical research.

Handwriting recognition includes layout analysis to 
to find text passages on a scanned page, as well as the 
actual recognition of the handwritten text. The envisaged application is based on the Transkribus platform (https://transkribus.eu/).

## Objectifs/Tâches

- Analysis, design, and implementation of a handwriting recognition system based on Transkribus
- Experimental evaluation of different optical models and language models
- Analysis, design, and implementation of a web application that demonstrates the results
- Writing of the Bachelor thesis
