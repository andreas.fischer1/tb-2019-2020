---
version: 1
titre: Mobile Digital Secretary
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
attribué à:
  - Luca De Laurentiis
professeurs co-superviseurs:
mots-clés: [document analysis, machine learning, pattern recognition]
langue: [F,E,D]
confidentialité: non
suite: oui
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth,height=\textheight]{img/digitalsecretary.jpg}
\end{center}
```

## Description/Contexte

Filling out physical forms over and over again is a time consuming and repetitive task. The mobile digital secretary is an intelligent mobile app that allows to take a picture of a document, identify its coresponding template, and fill out personal data automatically. The document can then be checked and completed digitally.

This Bachelor project continues the development of an application from a previous semester project.

## Objectifs/Tâches

- Analysis, design, and implementation of a document analysis system that identifies a document template based on a picture taken with the smartphone
- Experimental evaluation of different neural network architectures for this document analysis task
- Analysis, design, and implementation of the mobile app
- Writing of the Bachelor thesis
